

Description
------------------
This pipeline is based on [snakemake](https://snakemake.readthedocs.io/en/stable/). It allows to go from raw data of your bulk NGS sequencingraw data until the final count matrix with QC plots along the way.

Installation
------------------
### Step 1: Download and install miniconda3
First you need to download and install miniconda3:

for linux
```
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

for mac os
```
curl https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh -o Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
```


### Step 2: Clone the workflow

Clone the worflow to the folder where you run your pipelines
```
git clone https://gitlab.lrz.de/ImmunoPhysio/bulkSeqPipe
```


### Step 3: Install snakemake

```
conda install -c bioconda -c conda-forge snakemake
```

Configuration
---------------------
Fill the config.yaml and samples.csv located in the templates folder and moved them to the root folder.



Running the pipeline
----------------------

`snakemake --cores X --use-conda`	

Running the pipeline on our cluster
----------------------

1. Create a folder in the target dir that will store logs from the cluster. The pathis given in the command you invoke the pipeline with. `logs/cluster`

2. cd into bulkSeqPipe folder

3. Run `snakemake --cluster 'sbatch -n {cluster.n}  -t {cluster.time} --clusters=mpp2 --output={cluster.output}' --jobs 47 --cluster-config templates/cluster.yaml --use-conda --local-cores 28  --directory /full/path/to/target/dir`