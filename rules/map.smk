"""Align the data with STAR."""

localrules: multiqc_star, merge_counts


rule STAR_align:
	input:
		fq1="data/{sample}_trimmed.fastq.gz",
		index=lambda wildcards: star_index_prefix + '_' + str(samples.loc[wildcards.sample,'read_length']) + '/SA'
	output:
		temp('data/{sample}/Aligned.out.bam')
	log:
		'data/{sample}/Log.final.out'
	params:
		extra="""--outReadsUnmapped Fastx\
			 	--outFilterMismatchNmax {}\
			 	--outFilterMismatchNoverLmax {}\
			 	--outFilterMismatchNoverReadLmax {}\
			 	--outFilterMatchNmin {}\
			 	--outFilterScoreMinOverLread {}\
			 	--outFilterMatchNminOverLread {}\
			 	--outSAMtype BAM Unsorted""".format(
				config['MAPPING']['STAR']['outFilterMismatchNmax'],
				config['MAPPING']['STAR']['outFilterMismatchNoverLmax'],
				config['MAPPING']['STAR']['outFilterMismatchNoverReadLmax'],
				config['MAPPING']['STAR']['outFilterMatchNmin'],
				config['MAPPING']['STAR']['outFilterMatchNminOverLread'],
				config['MAPPING']['STAR']['outFilterScoreMinOverLread'],),
		index=lambda wildcards: star_index_prefix + '_' + str(samples.loc[wildcards.sample,'read_length']) + '/'	
	threads: 24
	wrapper:
		"0.36.0/bio/star/align"

rule sort_sam:
	input:
		'data/{sample}/Aligned.out.bam'
	params:
		TMPDIR=config['LOCAL']['temp-directory'],
		picard="$CONDA_PREFIX/share/picard-2.14.1-0/picard.jar",
		memory=config['LOCAL']['memory']	
	output:
		temp('data/{sample}.Aligned.sorted.bam')
	conda: '../envs/picard.yaml'
	shell:
		"""java -Xmx{params.memory} -jar -Djava.io.tmpdir={params.TMPDIR} {params.picard} SortSam\
		INPUT={input}\
		OUTPUT={output}\
		SORT_ORDER=coordinate\
		TMP_DIR={params.TMPDIR}
		"""

rule gene_count_single:
	input: 
		'data/{sample}.Aligned.sorted.bam'
	params:
		gtf = annotation_file
	conda: '../envs/htseq.yaml'
	output:
		'summary/{sample}_counts.csv'
	shell:
		"""htseq-count -f bam --additional-attr gene_name -m union -a 10 -t exon --stranded=no {input} {params.gtf} > {output}"""

rule multiqc_star:
	input:
		expand('data/{sample}/Log.final.out', sample=samples.index)
	output:
		html='reports/star.html'
	params: '-m star'
	wrapper:
		'0.21.0/bio/multiqc'


rule merge_counts:
	input:
		expand('summary/{sample}_counts.csv', sample=samples.index)
	params:
		sample_names=lambda wildcards: samples.index
	conda: '../envs/merge.yaml'
	output:
		counts='summary/counts_expression_matrix.tsv',
		gene_meta='summary/gene_annotation.csv'
	script:
		"../scripts/merge_counts_single.R"