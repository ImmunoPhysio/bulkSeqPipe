localrules: PCA_data, PCA_report

rule PCA_data:
	input:
		expression = 'summary/counts_expression_matrix.tsv',
		design = 'design.csv'
	output:
		rdata='data/PCA.RData',
		tsv='summary/filtered_counts.tsv'
	conda:
		'../envs/PCA.yaml'
	script:
		'../scripts/PCA_data.R'

rule PCA_report:
	input:
		pca_data = 'data/PCA.RData'
	output:
		'reports/PCA_report.html'
	conda:
		'../envs/PCA.yaml'
	script:
		'../scripts/PCA.Rmd'
