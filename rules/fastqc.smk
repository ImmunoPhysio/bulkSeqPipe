"""Get fastqc reports"""
localrules: multiqc_trimmomatic, fastqc_se, fastqc_pe, multiqc_fastqc_reads
ruleorder: fastqc_pe>fastqc_se

rule fastqc_se:
	"""Create fastqc reports"""
	input: 
		'data/{sample}_R1_001.fastq.gz'
	output:
		html = 'logs/{sample}_R1_fastqc.html',
		zip = 'logs/{sample}_R1_fastqc.zip'
	params: '--extract'
	wrapper:
		'0.21.0/bio/fastqc'

rule fastqc_pe:
	"""Create fastqc reports"""
	input: 
		R1 = 'data/{sample}_R1_001.fastq.gz',
		R2 = 'data/{sample}_R2_001.fastq.gz'
	output:
		html = 'logs/{sample}_R1_fastqc.html',
		zip = 'logs/{sample}_R1_fastqc.zip'
	shell:
		"""fastqc -t 2 {input.R1} {input.R2} --extract -o logs"""


rule multiqc_fastqc_reads:
	input: expand('logs/{sample}_R1_fastqc.html', sample=samples.index)
	output:
		html='reports/fastqc.html',
		data = 'reports/fastqc_data/multiqc_general_stats.txt'
	params: '-m fastqc'
	wrapper:
		'0.21.0/bio/multiqc'