localrules: create_star_index

import math
"""Generate all the meta data files"""

def get_sjdbOverhang(wildcards):
	return(int(wildcards.read_length)-1)

def get_genomeChrBinNbits(file):
	genomeLength = shell("wc -c {} | cut -d' ' -f1".format(file), iterable=True)
	genomeLength = int(next(genomeLength))
	referenceNumber = shell('grep "^>" {} | wc -l'.format(file), iterable=True)
	referenceNumber = int(next(referenceNumber))
	return(min([18,int(math.log2(genomeLength/referenceNumber))]))


rule create_star_index:
	input:
		reference_file=reference_file,
		annotation_file=annotation_file
	params:
		sjdbOverhang=lambda wildcards: get_sjdbOverhang(wildcards),
		genomeDir='{star_index_prefix}_{read_length}',
		genomeChrBinNbits=get_genomeChrBinNbits(reference_file)
	output:
		'{star_index_prefix}_{read_length}/SA'
	threads: 28
	conda: '../envs/star.yaml'
	shell:
		"""mkdir -p {params.genomeDir}; STAR\
		--runThreadN 4\
		--runMode genomeGenerate\
		--genomeDir {params.genomeDir}\
		--genomeFastaFiles {input.reference_file}\
		--sjdbGTFfile {input.annotation_file}\
		--limitGenomeGenerateRAM 30000000000\
		--sjdbOverhang {params.sjdbOverhang}\
		--genomeChrBinNbits {params.genomeChrBinNbits}
		"""