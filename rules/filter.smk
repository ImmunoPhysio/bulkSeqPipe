"""Filter data"""
localrules: multiqc_trimmomatic

rule trim_single:
	input:
		data='data/{sample}_R1_001.fastq.gz'
	output:
		data='data/{sample}_trimmed.fastq.gz'
	log:
		'logs/{sample}_trimlog.txt'
	params:
		trimmer=['LEADING:{} TRAILING:{} SLIDINGWINDOW:{}:{} MINLEN:{} ILLUMINACLIP:{}:{}:{}:{}'.format(
			config['FILTER']['trimmomatic']['LEADING'],
			config['FILTER']['trimmomatic']['TRAILING'],
			config['FILTER']['trimmomatic']['SLIDINGWINDOW']['windowSize'],
			config['FILTER']['trimmomatic']['SLIDINGWINDOW']['requiredQuality'],
			config['FILTER']['trimmomatic']['MINLEN'],
			config['FILTER']['trimmomatic']['adapters-file'],
			config['FILTER']['trimmomatic']['ILLUMINACLIP']['seedMismatches'],
			config['FILTER']['trimmomatic']['ILLUMINACLIP']['palindromeClipThreshold'],
			config['FILTER']['trimmomatic']['ILLUMINACLIP']['simpleClipThreshold'])],
		extra='-threads 10'
	threads: 10
	wrapper:
		'0.21.0/bio/trimmomatic/se'




rule multiqc_trimmomatic:
	input:
		expand('logs/{sample}_trimlog.txt', sample=samples.index)
	params: '-m trimmomatic'
	output:
		html = 'reports/filter.html'
	wrapper:
		'0.21.0/bio/multiqc'