import pandas as pd
import os


# Load configuration file
configfile: "config.yaml"
config['version'] = '0.1'

# Get sample names from samples.csv
samples = pd.read_table("samples.csv", header=0, sep=',', index_col=0)

# Get read_lengths from samples.csv
read_lengths = list(samples.loc[:,'read_length'])

# Constraint sample names wildcards
wildcard_constraints:
    sample="({})".format("|".join(samples.index))

# Create reference files prefixes
reference_prefix = os.path.join(config['META']['reference-directory'], config['META']['reference-file'].split('.fasta')[0])
annotation_prefix = os.path.join(config['META']['reference-directory'],config['META']['annotation-file'].split('.gtf')[0])
reference_file = os.path.join(config['META']['reference-directory'], config['META']['reference-file'])
annotation_file = os.path.join(config['META']['reference-directory'], config['META']['annotation-file'])
annotation_reduced_file = os.path.join(config['META']['reference-directory'],'.'.join([config['META']['annotation-file'].split('.gtf')[0],'reduced','gtf']))
star_index_prefix = os.path.join(config['META']['reference-directory'],'STAR_INDEX/SA')


rule all:
    input:
        #meta
        expand('{star_index_prefix}_{read_length}/SA', star_index_prefix=star_index_prefix, read_length=read_lengths),
        #qc
        expand('logs/{sample}_R1_fastqc.html', sample=samples.index),
        'reports/fastqc.html',
        'reports/fastqc_data/multiqc_general_stats.txt',
        #filter
        expand('data/{sample}_trimmed.fastq.gz', sample=samples.index),
        'reports/filter.html',
        #mapping
        expand('summary/{sample}_counts.csv', sample=samples.index),
        'reports/star.html',
        'summary/counts_expression_matrix.tsv'
        
rule meta:
    input:
        expand('{star_index_prefix}_{read_length}/SA', star_index_prefix=star_index_prefix, read_length=read_lengths)

rule qc:
    input:
        expand('logs/{sample}_R1_fastqc.html', sample=samples.index),
        'reports/fastqc.html',
        'reports/fastqc_data/multiqc_general_stats.txt'

rule filter:
    input:
        expand('data/{sample}_trimmed.fastq.gz', sample=samples.index),
        'reports/filter.html'
        
rule map:
    input:  
        expand('summary/{sample}_counts.csv', sample=samples.index),
        'reports/star.html'

rule PCA:
    input:
        'reports/PCA_report.html',
        'summary/filtered_counts.tsv',
        'summary/gene_annotation.csv'

include: "rules/generate_meta.smk"
include: "rules/fastqc.smk"
include: "rules/filter.smk"
include: "rules/map.smk"
include: "rules/PCA.smk"
