library(tidyr)
library(DESeq2)
library(gtools)

expression = read.csv(file = snakemake@input$expression, sep = '\t', row.names = 1, header = TRUE, check.names = FALSE)
design = data.frame(read.csv(snakemake@input$design, row.names = 1, header = TRUE, check.names = FALSE, stringsAsFactors = FALSE))

samples = mixedsort(rownames(design))
expression = expression[,mixedsort(rownames(design))]
design = data.frame(design[mixedsort(rownames(design)),])

rownames(design) = samples
#design$groups = as.vector(unite(design, sep='_')[1])
if(ncol(design)==1){
	combined = design
	colnames(combined) = 'groups'
}else{
	combined = unite(design, 'groups', 1:ncol(design))
}
design_formula = as.formula('~ 0 + groups')
dds = DESeqDataSetFromMatrix(expression, colData = combined, design = design_formula)

keep <- rowSums(counts(dds)) >= ncol(counts(dds))
dds <- dds[keep,]
vst = vst(dds)
rm = rowMads(assay(vst))

select <- rm > summary(rm)[5]

PCA <- prcomp(t(assay(vst)[select, ]), scale. = TRUE, center = TRUE)
plot_data = cbind(PCA$x, design, combined)
save(plot_data,select, file=snakemake@output$rdata)
write.table(expression[keep,], file=snakemake@output$tsv, quote=FALSE, sep='\t')
